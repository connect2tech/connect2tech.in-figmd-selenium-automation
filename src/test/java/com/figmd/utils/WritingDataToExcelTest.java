package com.figmd.utils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.c2t.util.WriteToExcel;
import com.figmd.login.LoginUsingPom;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class WritingDataToExcelTest {

	WebDriver driver;
	LoginUsingPom pom;
	final static Logger logger = Logger.getLogger(WritingDataToExcelTest.class);

	@BeforeTest
	public void before() {
		String url = "https://pegasus-demo.figmd.com/login";
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-FIGmd-Selenium-Automation/chromedriver_win32_2.45/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(url);
		pom = new LoginUsingPom(driver);

	}

	@Test(priority = 1)
	public void login() {

		pom.login();
	}

	@Parameters(value={"file_name"})
	@Test(priority = 3)
	public void writeDataToExcel(String fileName) {

		List<WebElement> dataTable__row = driver.findElements(By.className("dataTable__row"));
		logger.debug("dataTable__row=>" + dataTable__row);
		logger.debug("dataTable__row.size()=>" + dataTable__row.size());

		WriteToExcel.writeToExcel(dataTable__row, fileName, "Practice_Measure");

	}

}
