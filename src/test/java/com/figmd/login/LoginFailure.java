package com.figmd.login;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class LoginFailure {
	
	WebDriver driver;
	
	@BeforeTest
	public void before(){
		String url = "https://pegasus-demo.figmd.com/login";
		System.setProperty("webdriver.chrome.driver", "D:/nchaurasia/Automation-Architect/connect2tech.in-FIGmd-Selenium-Automation/chromedriver_win32_2.45/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(url);
	}
	
	@Test(priority=1)
	public void login(){
		
		driver.findElement(By.id("username")).sendKeys("");
		driver.findElement(By.name("password")).sendKeys("Wrong");
		driver.findElement(By.xpath("//*[@id='home']/div/div/div/div[2]/div[4]/button")).click();
		
	}
	
	@Test(priority=2)
	public void loginFailure(){
		String expectedText = "Incorrect username or password.The maximum retry attempts allowed for login are 3. Your account will be locked if you exceed 3 attempts.";
		String actualText =  driver.findElement(By.xpath("//*[@id='home']/div/div/div/div[2]/div[2]/div/p")).getText();
		Assert.assertEquals(actualText, expectedText);
	
	}
	

}
