package com.c2t.tabs.framework2;

import org.openqa.selenium.WebDriver;

import com.figmd.login.LoginUsingPom;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class DriverSingleton {

	static DriverSingleton singleton;
	static WebDriver driver;
	static LoginUsingPom pom;

	private DriverSingleton() {
	}

	public static DriverSingleton getSingletion() {
		return singleton;
	}

	public static void setDriver(WebDriver d) {
		driver = d;
	}

	public static void setLoginPom(LoginUsingPom pom1) {
		pom = pom1;
		;
	}

	public static WebDriver getDriver() {
		return driver;
	}

	public static LoginUsingPom getLoginPom() {
		return pom;
	}
}
