package com.c2t.tabs.framework2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.c2t.util.ReadFromExcel;
import com.figmd.login.LoginUsingPom;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class HomePagePracticeTabIdMeasureValidation_Praticse3_3 {

	WebDriver driver;
	LoginUsingPom pom;
	final static Logger logger = Logger.getLogger(HomePagePracticeTabIdMeasureValidation_Praticse3_3.class);

	@BeforeTest
	public void before() {
		String url = "https://pegasus-demo.figmd.com/login";
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-FIGmd-Selenium-Automation/chromedriver_win32_2.45/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(url);
		pom = new LoginUsingPom(driver);

	}

	@Test(priority = 1)
	public void login() {

		pom.login();
	}

	@Test(priority = 2)
	public void isFocusOnPractiseTab() {

		String expected = "true";

		// mastertab__tab__tab-content
		List<WebElement> list = driver.findElements(By.className("mastertab__tab__tab-content"));
		logger.debug("list=>" + list);
		logger.debug("list.size()=>" + list.size());

		String aria_selected = list.get(0).getAttribute("aria-selected");
		logger.debug("aria_selected=>" + aria_selected);
		Assert.assertEquals(aria_selected, expected);

	}

	@Test(priority = 4)
	public void chooseDropDownPractise3() {
		WebElement dropDown = driver.findElement(By.xpath("//*[@id='root']/div/div/div/div/main/div[2]/div[2]/div[1]/div[2]"));
		dropDown.click();
		WebElement chooseDropDown = driver.findElement(By.xpath("//*[@id='menu-practice']/div[2]/ul/li[3]"));
		chooseDropDown.click();
	}

	@Test(priority = 5, enabled = true)
	public void validateMeasureIdMeasureNameCombinations() {

		List<WebElement> dataTable__row = driver.findElements(By.className("dataTable__row"));
		logger.debug("dataTable__row=>" + dataTable__row);
		logger.debug("dataTable__row.size()=>" + dataTable__row.size());

		// WriteToExcel.writeToExcel(dataTable__row,"FIGmd.xlsx","Practice_Measure");

		Map<String, String> actual = storeIdPractiseData(dataTable__row);
		Map<String, String> expected = ReadFromExcel.readFromExcel(
				"D:/nchaurasia/Automation-Architect/connect2tech.in-FIGmd-Selenium-Automation/FIGmd.xlsx",
				"Practice_Measure");

		logger.debug("expected=>" + expected);
		logger.debug("actual=>" + actual);
		Assert.assertEquals(actual, expected);

	}

	public Map<String, String> storeIdPractiseData(List<WebElement> dataTable__row) {

		Map<String, String> id_measure_map = new HashMap<String, String>();

		for (int i = 0; i < dataTable__row.size(); i++) {
			WebElement measure_id = dataTable__row.get(i).findElement(By.className("measure-id"));
			WebElement measure_name = dataTable__row.get(i).findElement(By.className("measure-name"));
			logger.debug("measure_id=>" + measure_id.getText());
			logger.debug("measure_name=>" + measure_name.getText());

			id_measure_map.put(measure_id.getText(), measure_name.getText());
		}

		return id_measure_map;
	}

}
