package com.c2t.tabs.framework1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.c2t.util.ReadFromExcel;
import com.c2t.util.WriteToExcel;
import com.figmd.login.LoginUsingPom;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Practise2 extends PractiseBefore{

	//final static Logger logger = Logger.getLogger(Practise2.class);

	Practise2(){
		//logger.debug("com.c2t.tabs.Practise2.Practise2()==>" + Thread.currentThread().getId());
	}

	@Test(priority = 1)
	public void login() {
		logger.debug("com.c2t.tabs.Practise2.login()==>" + Thread.currentThread().getId());
	}

}
