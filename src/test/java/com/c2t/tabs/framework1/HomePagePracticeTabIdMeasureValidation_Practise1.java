package com.c2t.tabs.framework1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.c2t.util.ReadFromExcel;
import com.c2t.util.WriteToExcel;
import com.figmd.login.LoginUsingPom;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class HomePagePracticeTabIdMeasureValidation_Practise1 extends HomePagePracticeTabIdMeasureValidation_Base {

	static Logger logger = Logger.getLogger(HomePagePracticeTabIdMeasureValidation_Practise1.class);
	
	@Test(priority = 0)
	public void setDriverFromSingleon() {

		logger.debug("com.c2t.tabs.framework1.HomePagePracticeTabIdMeasureValidation_Practise1.setDriverFromSingleon()");
		driver = DriverSingleton.getDriver();
		logger.debug("driver=>" + driver);

	}

	@Test(priority = 1)
	public void login() {
		driver.findElement(By.id("username")).sendKeys("");
		driver.findElement(By.name("password")).sendKeys("Abcd@1234");
		driver.findElement(By.xpath("//*[@id='home']/div/div/div/div[2]/div[4]/button")).click();

	}

	@Test(priority = 2)
	public void isFocusOnPractiseTab() {

		String expected = "true";

		// mastertab__tab__tab-content
		List<WebElement> list = driver.findElements(By.className("mastertab__tab__tab-content"));
		logger.debug("list=>" + list);
		logger.debug("list.size()=>" + list.size());

		String aria_selected = list.get(0).getAttribute("aria-selected");
		logger.debug("aria_selected=>" + aria_selected);
		Assert.assertEquals(aria_selected, expected);

	}

	@Test(priority = 3)
	public void validateMeasureIdMeasureNameCombinations() {

		List<WebElement> dataTable__row = driver.findElements(By.className("dataTable__row"));
		logger.debug("dataTable__row=>" + dataTable__row);
		logger.debug("dataTable__row.size()=>" + dataTable__row.size());

		// WriteToExcel.writeToExcel(dataTable__row,"FIGmd.xlsx","Practice_Measure");

		Map<String, String> actual = storeIdPractiseData(dataTable__row);
		Map<String, String> expected = ReadFromExcel.readFromExcel(
				"D:/nchaurasia/Automation-Architect/connect2tech.in-FIGmd-Selenium-Automation/FIGmd.xlsx",
				"Practice_Measure");

		logger.debug("expected=>" + expected);
		logger.debug("actual=>" + actual);
		Assert.assertEquals(actual, expected);

	}

}
