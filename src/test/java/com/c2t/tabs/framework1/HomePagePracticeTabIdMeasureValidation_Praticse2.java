package com.c2t.tabs.framework1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.c2t.util.ReadFromExcel;
import com.figmd.login.LoginUsingPom;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class HomePagePracticeTabIdMeasureValidation_Praticse2 extends HomePagePracticeTabIdMeasureValidation_Base {

	static Logger logger = Logger.getLogger(HomePagePracticeTabIdMeasureValidation_Praticse2.class);
	
	@Test(priority = 0)
	public void getDriverFromSingleon() {
		
		logger.debug("com.c2t.tabs.framework1.HomePagePracticeTabIdMeasureValidation_Praticse2.getDriverFromSingleon()");
		driver = DriverSingleton.getDriver();
		logger.debug("driver=>" + driver);

	}

	@Test(priority = 2)
	public void chooseDropDownPractise2() {
		WebElement dropDown = driver
				.findElement(By.xpath("//*[@id='root']/div/div/div/div/main/div[2]/div[2]/div[1]/div[2]"));
		dropDown.click();
		WebElement chooseDropDown = driver.findElement(By.xpath("//*[@id='menu-practice']/div[2]/ul/li[2]"));
		chooseDropDown.click();
	}

	@Test(priority = 3)
	public void validateMeasureIdMeasureNameCombinations() {

		List<WebElement> dataTable__row = driver.findElements(By.className("dataTable__row"));
		logger.debug("dataTable__row=>" + dataTable__row);
		logger.debug("dataTable__row.size()=>" + dataTable__row.size());

		// WriteToExcel.writeToExcel(dataTable__row,"FIGmd.xlsx","Practice_Measure");

		Map<String, String> actual = storeIdPractiseData(dataTable__row);
		Map<String, String> expected = ReadFromExcel.readFromExcel(
				"D:/nchaurasia/Automation-Architect/connect2tech.in-FIGmd-Selenium-Automation/FIGmd.xlsx",
				"Practice_Measure");

		logger.debug("expected=>" + expected);
		logger.debug("actual=>" + actual);
		Assert.assertEquals(actual, expected);

	}

}
