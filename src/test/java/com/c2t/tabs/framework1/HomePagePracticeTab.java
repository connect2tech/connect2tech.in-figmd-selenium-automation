package com.c2t.tabs.framework1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class HomePagePracticeTab {

	WebDriver driver;

	@BeforeTest
	public void before() {
		String url = "https://pegasus-demo.figmd.com/login";
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-FIGmd-Selenium-Automation/chromedriver_win32_2.45/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(url);
	}

	@Test(priority = 1)
	public void login() {

		driver.findElement(By.id("username")).sendKeys("");
		driver.findElement(By.name("password")).sendKeys("");
		driver.findElement(By.xpath("//*[@id='home']/div/div/div/div[2]/div[4]/button")).click();

	}

	@Test(priority = 2)
	public void isFocusOnPractiseTab() {
		
		int expectedColCount = 4;
		
		WebElement practiseMeasureSetWebElement = driver.findElement(By.cssSelector("div.jss1.jss25"));
		List<WebElement> columnsWebElement = practiseMeasureSetWebElement.findElements(By.tagName("div"));
		int actualColumnCount = columnsWebElement.size();
		Assert.assertEquals(actualColumnCount, expectedColCount);
		

		
	}

}
