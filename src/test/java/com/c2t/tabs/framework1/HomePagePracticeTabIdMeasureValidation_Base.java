package com.c2t.tabs.framework1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;

import com.figmd.login.LoginUsingPom;
import com.figmd.utils.PractiseData;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class HomePagePracticeTabIdMeasureValidation_Base {

	WebDriver driver;
	LoginUsingPom pom;
	static Logger logger_base = Logger.getLogger(HomePagePracticeTabIdMeasureValidation_Base.class);

	@BeforeTest
	public void before() {

		logger_base.debug("com.c2t.tabs.framework1.HomePagePracticeTabIdMeasureValidation_Base.before()");
		String url = "https://pegasus-demo.figmd.com/login";
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-FIGmd-Selenium-Automation/chromedriver_win32_2.45/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(url);
		pom = new LoginUsingPom(driver);
		DriverSingleton.setDriver(driver);
		DriverSingleton.setLoginPom(pom);

	}

	public Map<String, String> storeIdPractiseData(List<WebElement> dataTable__row) {

		Map<String, String> id_measure_map = new HashMap<String, String>();

		for (int i = 0; i < dataTable__row.size(); i++) {
			WebElement measure_id = dataTable__row.get(i).findElement(By.className("measure-id"));
			WebElement measure_name = dataTable__row.get(i).findElement(By.className("measure-name"));
			// logger_base.debug("measure_id=>" + measure_id.getText());
			// logger_base.debug("measure_name=>" + measure_name.getText());

			// List<WebElement> mark_child__percent = dataTable__row.get(i)
			// .findElements(By.className("mark-child mark-child__percent"));

			id_measure_map.put(measure_id.getText(), measure_name.getText());
		}

		return id_measure_map;
	}

	public List<PractiseData> storeIdPractiseDataObjectInList(List<WebElement> dataTable__row) {

		List<PractiseData> list = new ArrayList<PractiseData>();

		for (int i = 0; i < dataTable__row.size(); i++) {

			PractiseData data = new PractiseData();

			WebElement measure_id = dataTable__row.get(i).findElement(By.className("measure-id"));
			WebElement measure_name = dataTable__row.get(i).findElement(By.className("measure-name"));
			WebElement registry__benchmark = dataTable__row.get(i).findElement(By.className("registry__benchmark"));
			WebElement cms__benchmark = dataTable__row.get(i).findElement(By.className("cms__benchmark"));
			WebElement performance__width__10 = dataTable__row.get(i)
					.findElement(By.className("performance__width--10"));

			logger_base.debug("measure_id=>" + measure_id.getText() + ",		measure_name=>" + measure_name.getText()
					+ ",		registry__benchmark=>" + registry__benchmark.getText() + ",		cms__benchmark=>"
					+ cms__benchmark.getText() + ",		performance__width__10=>" + performance__width__10.getText());

			data.setMeasure_id(measure_id.getText());
			data.setMeasure_name(measure_name.getText());
			data.setRegistry__benchmark(registry__benchmark.getText());
			data.setCms__benchmark(cms__benchmark.getText());
			data.setPerformance__width__10(performance__width__10.getText());
			
			list.add(data);

		}
		return list;
	}

}
