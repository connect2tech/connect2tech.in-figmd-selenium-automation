package com.figmd.login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class LoginUsingPom {
	WebDriver driver;

	public LoginUsingPom(WebDriver d) {
		driver = d;
	}

	public void login() {

		driver.findElement(By.id("username")).sendKeys("");
		driver.findElement(By.name("password")).sendKeys("");
		driver.findElement(By.xpath("//*[@id='home']/div/div/div/div[2]/div[4]/button")).click();

	}
}
