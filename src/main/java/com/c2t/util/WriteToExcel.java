package com.c2t.util;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


//import statements
public class WriteToExcel {

	final static Logger logger = Logger.getLogger(WriteToExcel.class);

	public static void writeToExcel(List<WebElement> dataTable__row, String workbookName, String sheetName) {
		// Blank workbook
		XSSFWorkbook workbook = new XSSFWorkbook();

		// Create a blank sheet
		XSSFSheet sheet = workbook.createSheet(sheetName);
		
		Row headerRow = sheet.createRow(0);
		Cell head_cell0 = headerRow.createCell(0);
		head_cell0.setCellValue("MEASURE-ID");

		Cell head_cell1 = headerRow.createCell(1);
		head_cell1.setCellValue("MEASURE");

		Cell head_cell2 = headerRow.createCell(2);
		head_cell2.setCellValue("REGISTRY-AVERAGE");
		
		Cell head_cell3 = headerRow.createCell(3);
		head_cell3.setCellValue("CMS-AVERAGE");
		
		Cell head_cell4 = headerRow.createCell(4);
		head_cell4.setCellValue("ACHIEVED-PERFORMANCE");
		

		for (int i = 0; i < dataTable__row.size(); i++) {
			WebElement measure_id = dataTable__row.get(i).findElement(By.className("measure-id"));
			WebElement measure_name = dataTable__row.get(i).findElement(By.className("measure-name"));
			WebElement registry__benchmark = dataTable__row.get(i).findElement(By.className("registry__benchmark"));
			WebElement cms__benchmark = dataTable__row.get(i).findElement(By.className("cms__benchmark"));
			WebElement performance__width__10 = dataTable__row.get(i).findElement(By.className("performance__width--10"));
			

			logger.debug("measure_id=>" + measure_id.getText() + ",		measure_name=>" + measure_name.getText()
					+ ",		registry__benchmark=>" + registry__benchmark.getText() + ",		cms__benchmark=>"
					+ cms__benchmark.getText() + ",		performance__width__10=>"+performance__width__10.getText());
			/*logger.debug("measure_name=>" + measure_name.getText());
			logger.debug("registry__benchmark=>" + registry__benchmark.getText());
			logger.debug("cms__benchmark=>" + cms__benchmark.getText());*/

			Row row = sheet.createRow(i+1);
			Cell cell0 = row.createCell(0);
			cell0.setCellValue(measure_id.getText());

			Cell cell1 = row.createCell(1);
			cell1.setCellValue(measure_name.getText());

			Cell cell2 = row.createCell(2);
			cell2.setCellValue(registry__benchmark.getText());
			
			Cell cell3 = row.createCell(3);
			cell3.setCellValue(cms__benchmark.getText());
			
			Cell cell4 = row.createCell(4);
			cell4.setCellValue(performance__width__10.getText());
			

		}

		try {
			// Write the workbook in file system
			FileOutputStream out = new FileOutputStream(new File(workbookName));
			workbook.write(out);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug(e.toString());
		}
	}

}