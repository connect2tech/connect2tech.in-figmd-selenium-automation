package com.c2t.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadFromExcel {

	final static Logger logger = Logger.getLogger(ReadFromExcel.class);

	public static Map<String, String> readFromExcel(String fileName, String sheetName) {
		Map<String, String> id_measure_map = new HashMap<String, String>();
		try {
			File file = new File(fileName);
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet sheet = workbook.getSheet(sheetName);

			Iterator<Row> rows = sheet.rowIterator();

			while (rows.hasNext()) {
				Row row = rows.next();

				String id = row.getCell(0).toString();
				String measure = row.getCell(1).toString();
				// logger.debug("id==>" + id);
				// logger.debug("measure==>" + measure);
				id_measure_map.put(id, measure);

			}

			logger.debug("id_measure_map==>" + id_measure_map);

		} catch (Exception e) {
			logger.error(e);
		}

		return id_measure_map;

	}

	public static Map<String, String> readFromExcelPractiseDataObjectInList(String fileName, String sheetName) {
		Map<String, String> id_measure_map = new HashMap<String, String>();
		try {
			File file = new File(fileName);
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet sheet = workbook.getSheet(sheetName);

			Iterator<Row> rows = sheet.rowIterator();

			int rowCount = 0;
			while (rows.hasNext()) {
				Row row = rows.next();

				// Don't read header
				if (rowCount == 0) {
					continue;
				}

				String id = row.getCell(0).toString();
				String measure = row.getCell(1).toString();
				String registry = row.getCell(2).toString();
				String cms = row.getCell(3).toString();
				String achieved = row.getCell(4).toString();
				logger.debug("id==>" + id + "		measure==>" + measure );

			}

			logger.debug("id_measure_map==>" + id_measure_map);

		} catch (Exception e) {
			logger.error(e);
		}

		return id_measure_map;

	}

}