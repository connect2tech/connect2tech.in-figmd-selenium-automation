package com.c2t.dp;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class SingletonPattern {
	int a;

	static SingletonPattern p = null;

	private SingletonPattern() {
	}

	public static SingletonPattern getSingleton() {
		if (p == null) {
			p = new SingletonPattern();
			return p;
		} else {
			return p;
		}
	}
}
